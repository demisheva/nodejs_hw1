const express = require('express');
const app = express();

fs = require('fs');
const path = require('path');

// Adding Swagger UI to serve api docs at http://localhost:8080/api-docs/
const swaggerUi = require('swagger-ui-express');
swaggerDocument = require('./openapi.json');
app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument)
);
// End Adding Swagger UI


// Adding a logger
const morgan = require('morgan')
app.use(morgan('combined'));
// End adding a logger

// Ping endpoint
app.get('/ping', (req, res) => res.send('pong'));
// End ping endpoint


app.use('/api/files', express.json()); // crucial to be able to read json in a request body

// Saving a file
app.post('/api/files', (req, res) => {
    const data = req.body;
    if (!data) {
        res.statusCode = 400;
        res.send({"message": "Please provide body"});
    } else if (!data.filename) {
        res.statusCode = 400;
        res.send({"message": "Please specify 'filename' parameter"});
    } else if (!data.content) {
        res.statusCode = 400;
        res.send({"message": "Please specify 'content' parameter"});
    } else {
        fs.writeFile(`./storage/${data.filename}`, data.content, function (err) {
            if (err) {
                res.statusCode = 500;
                res.send({"message": err});
            }
            res.send({"message": "File created successfully"});
        });
    }
});

app.get('/api/files', (req, res) => {
    fs.readdir('./storage', (err, files) => {
        if (err) {
            res.statusCode = 500;
            res.send({"message": err});
        } else {
            res.send({"message": "Success", "files": files});
        }
    })
});

app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename;
    console.log(filename);

    fs.stat(`./storage/${filename}`, (err, stats) => {
        if (err) {
            if (err.code === 'ENOENT') {
                res.statusCode = 400;
                res.send({"message": `No file with '${filename}' filename found`});
            } else {
                res.statusCode = 500;
                res.send({"message": err});
            }
        } else {
            const updated = stats.mtime;
            fs.readFile(`./storage/${filename}`,  'utf8', (err, content) => {
                if (err) {
                    res.statusCode = 500;
                    res.send({"message": err});
                } else {
                    res.send({
                        "message": "Success",
                        "filename": filename,
                        "content": content,
                        "extension": path.extname(filename).slice(1),
                        "uploadedDate": updated
                    });
                }
            });
        }
    });
});

app.listen(8080);
